package com.example.planme

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.provider.BaseColumns
import android.view.View
import android.widget.DatePicker
import android.widget.TimePicker
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.planme.database.DataContract
import com.example.planme.database.ReaderDbHelper
import kotlinx.android.synthetic.main.activity_detail.*
import java.util.*

class DetailActivity : AppCompatActivity(), DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener{

    lateinit var dbHelper: ReaderDbHelper
    lateinit var db : SQLiteDatabase
    lateinit var data : DataModel

    var day = 0
    var month = 0
    var year = 0
    var hour = 0
    var minute = 0

    var savedDay = 0
    var savedMonth = 0
    var savedYear = 0
    var savedHour = 0
    var savedMinute = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        dbHelper = ReaderDbHelper(this)
        db = dbHelper.writableDatabase

        var status = intent.getBooleanExtra("status", false)
        if (!status) {
            pickerDate()

            data = intent.getParcelableExtra("data")

            displayData(data)

            btn_delete.visibility = View.VISIBLE
            btn_save.text = "Ubah Tugas"
            initListenerEdit()
        } else {
            pickerDate()
            initListener()
        }
    }

    private fun displayData(data: DataModel?) {
        val dateTime = this.data.dateTime

        et_title.setText(this.data.title)
        et_desc.setText(this.data.desc)
        if (dateTime != null) {
            val date = dateTime.split("+")[0].split("-")
            val tvDate = "${date[2]}-${date[1]}-${date[0]}"

            tv_date.text = tvDate
        }
        if (dateTime != null) {
            tv_time.text = dateTime.split("+")[1]
        }

    }

    private fun getDateTimeCalender() {
        val cal : Calendar = Calendar.getInstance()
        day = cal.get(Calendar.DAY_OF_MONTH)
        month = cal.get(Calendar.MONTH)
        year = cal.get(Calendar.YEAR)
        hour = cal.get(Calendar.HOUR)
        minute = cal.get(Calendar.MINUTE)
    }

    private fun pickerDate() {
        btn_date.setOnClickListener {
            getDateTimeCalender()
            DatePickerDialog(this, this, year, month, day).show()
        }
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        savedDay = dayOfMonth
        savedMonth = month
        savedYear = year

        getDateTimeCalender()
        TimePickerDialog(this, this, hour, minute, true).show()
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        savedHour = hourOfDay
        savedMinute = minute

        tv_date.text = "$savedDay-$savedMonth-$savedYear"
        tv_time.text = "$savedHour:$savedMinute"
    }


    private fun initListener() {
        btn_save.setOnClickListener{
            var sTitle = et_title.text.toString()
            var sDesc = et_desc.text.toString()
            var sDateTime = "$savedYear-$savedMonth-$savedDay+$savedHour:$savedMinute"

            if (sTitle.isNullOrEmpty()) {
                et_title.error = "Silahkan masukkan title"
                et_title.requestFocus()
            }  else if (savedYear == 0 || savedMonth == 0 || savedDay == 0 || savedMinute == null || savedHour == null) {
                Toast.makeText(this, "Tolong isi waktu", Toast.LENGTH_SHORT).show()
            }else {
                val values = ContentValues().apply {
                    put(DataContract.DataEntry.COLUMN_NAME_TITLE, sTitle)
                    put(DataContract.DataEntry.COLUMN_NAME_DESC, sDesc)
                    put(DataContract.DataEntry.COLUMN_DATE_TIME, sDateTime)
                }

                val newRowid = db.insert(DataContract.DataEntry.TABLE_NAME, null, values)
                if (newRowid == -1L) {
                    Toast.makeText(this, "Data gagal di simpan", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Data telah di simpan", Toast.LENGTH_SHORT).show()
                    finish()
                }
            }
        }
    }

    private fun initListenerEdit() {
        btn_delete.setOnClickListener {
            val selection = "${BaseColumns._ID} LIKE ?"
            val selectionArg = arrayOf(data.id.toString())

            val deleteRows = db.delete(DataContract.DataEntry.TABLE_NAME, selection, selectionArg)
            if (deleteRows == -1) {
                Toast.makeText(this, "Data gagal di hapus", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Data telah di hapus", Toast.LENGTH_SHORT).show()
                finish()
            }
        }

        btn_save.setOnClickListener{
            var sTitle = et_title.text.toString()
            var sDesc = et_desc.text.toString()
            var sDateTime = "$savedYear-$savedMonth-$savedDay+$savedHour:$savedMinute"

            if (sTitle.isNullOrEmpty()) {
                et_title.error = "Silahkan masukkan title"
                et_title.requestFocus()
            } else if (sDesc.isNullOrEmpty()) {
                et_desc.error = "Silahkan masukkan desc"
                et_desc.requestFocus()
            } else {

                val selection = "${BaseColumns._ID} LIKE ?"
                val selectionArg = arrayOf(data.id.toString())

                val values = ContentValues().apply {
                    put(DataContract.DataEntry.COLUMN_NAME_TITLE, sTitle)
                    put(DataContract.DataEntry.COLUMN_NAME_DESC, sDesc)
                    put(DataContract.DataEntry.COLUMN_DATE_TIME, sDateTime)
                }

                val editRowid = db.update(DataContract.DataEntry.TABLE_NAME, values, selection, selectionArg)
                if (editRowid == -1) {
                    Toast.makeText(this, "Data gagal di ubah", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Data telah di ubah", Toast.LENGTH_SHORT).show()
                    finish()
                }
            }
        }
    }

}