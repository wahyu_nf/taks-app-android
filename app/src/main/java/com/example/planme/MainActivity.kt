package com.example.planme

import android.content.Intent
import android.os.Bundle
import android.provider.BaseColumns
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.planme.database.DataContract
import com.example.planme.database.ReaderDbHelper
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), DataAdapter.Callback {

  lateinit var dbHelper: ReaderDbHelper

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    dbHelper = ReaderDbHelper(this)
    getData()

    initListener()
  }

  private fun getData() {
    val db = dbHelper.readableDatabase

    val projection = arrayOf(
      BaseColumns._ID,
      DataContract.DataEntry.COLUMN_NAME_TITLE,
      DataContract.DataEntry.COLUMN_NAME_DESC,
      DataContract.DataEntry.COLUMN_DATE_TIME
    )

    var cursor = db.query(
      DataContract.DataEntry.TABLE_NAME,
      projection,
      null,
      null,
      null,
      null,
      null
    )

    val dataList = mutableListOf<DataModel>()
    with(cursor) {
      while (moveToNext()) {
        val id = getLong(getColumnIndex(BaseColumns._ID))
        val title = getString(getColumnIndexOrThrow(DataContract.DataEntry.COLUMN_NAME_TITLE))
        val desc = getString(getColumnIndexOrThrow(DataContract.DataEntry.COLUMN_NAME_DESC))
        val dateTime = getString(getColumnIndexOrThrow(DataContract.DataEntry.COLUMN_DATE_TIME))

        dataList.add(DataModel(id, title, desc, dateTime))
      }
    }

    var dataAdapter = DataAdapter(this)
    rv_data.apply {
      var linearLayoutManager = LinearLayoutManager(context)
      linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
      layoutManager = linearLayoutManager
      adapter = dataAdapter
    }

    dataAdapter.setData(dataList)
    dataAdapter.notifyDataSetChanged()
  }

  private fun initListener() {
    fab_add.setOnClickListener {
      startActivity(Intent(this, DetailActivity::class.java).putExtra("status", true))
    }
  }

  private fun getDataDummy() {
    var dataList = ArrayList<DataModel>()
    dataList.add(DataModel(1, "Belajar Java", "Hari Akan Sampai ke Variable"))
    dataList.add(DataModel(1, "Belajar Java", "Hari Akan Sampai ke Variable"))
    dataList.add(DataModel(1, "Belajar Java", "Hari Akan Sampai ke Variable"))
    dataList.add(DataModel(1, "Belajar Java", "Hari Akan Sampai ke Variable"))
    dataList.add(DataModel(1, "Belajar Java", "Hari Akan Sampai ke Variable"))

    var dataAdapter = DataAdapter(this)
    rv_data.apply {
      var linearLayoutManager = LinearLayoutManager(context)
      linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
      layoutManager = linearLayoutManager
      adapter = dataAdapter
    }

    dataAdapter.setData(dataList)
    dataAdapter.notifyDataSetChanged()
  }

  //  to enter activity edit
  override fun onClick(data: DataModel) {
    startActivity(
      Intent(this, DetailActivity::class.java)
        .putExtra("status", false)
        .putExtra("data", data)
    )
  }

  override fun onResume() {
    super.onResume()

    getData()
  }
}