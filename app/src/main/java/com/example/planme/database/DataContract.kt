package com.example.planme.database

import android.provider.BaseColumns

object DataContract {

  object DataEntry : BaseColumns {
    const val TABLE_NAME = "entry"
    const val COLUMN_NAME_TITLE = "title"
    const val COLUMN_NAME_DESC = "desc"
    const val COLUMN_DATE_TIME = "dateTime"
  }

}