package com.example.planme.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns

class ReaderDbHelper(context: Context) :
  SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

  private val SQL_CREATE_ENTRIES = "CREATE TABLE ${DataContract.DataEntry.TABLE_NAME} (" +
          "${BaseColumns._ID} INTEGER PRIMARY KEY," +
          "${DataContract.DataEntry.COLUMN_NAME_TITLE} TEXT," +
          "${DataContract.DataEntry.COLUMN_NAME_DESC} TEXT," +
          "${DataContract.DataEntry.COLUMN_DATE_TIME} TEXT)"

  override fun onCreate(db: SQLiteDatabase) {
    db.execSQL(SQL_CREATE_ENTRIES)
  }

  override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

  }

  companion object {
    const val DATABASE_NAME = "taks.db"
    const val DATABASE_VERSION = 4
  }

}