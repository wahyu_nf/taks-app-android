package com.example.planme

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build

class BaseApplications: Application() {

  companion object {
    const val CHANNEL_1_ID = "channel1"
  }

  override fun onCreate() {
    super.onCreate()

    createNotificationChannels()
  }

  private fun createNotificationChannels() {
    if (Build.VERSION.SDK_INT >=  Build.VERSION_CODES.O) {
      val channel1 = NotificationChannel(
        CHANNEL_1_ID,
        "Channel Tugas",
        NotificationManager.IMPORTANCE_HIGH
      )
      channel1.description = "Channel Notif Tugas"

      val manager = getSystemService(NotificationManager::class.java)
      manager?.createNotificationChannel(channel1)
    }
  }
}
